import json
from urllib.parse import urljoin

import requests
from django.conf import settings
from django.core.management.base import BaseCommand
from django.template import loader
from requests.auth import HTTPBasicAuth

from dit_enterprisehub_monitoring.models_icinga_director import IcingaHost
from dit_enterprisehub_monitoring.models_icinga_ido import CusGroups
from dit_enterprisehub_monitoring.models_mon_collect import *


class Command(BaseCommand):
    help = "Sync verinice objects"

    api_url = settings.VERINICE_URL + "veriniceserver/sync/sync"

    customer = "c100005"

    def sync_clients(self):
        all_clients = (
            CollectAppsRel.objects.using("mon_collect")
            .filter(
                computer_id__istartswith=self.customer, computer_id__icontains="cwde"
            )
            .values_list("computer_id", flat=True)
            .distinct()
        )

        client_list = list()

        soap_tpl = loader.get_template("soap_verinice_client.xml")

        for client in all_clients:
            host = self.get_or_create_mon_client(client)

            template_opts = dict()
            template_opts["verinice_source_id"] = "mon_" + self.customer
            template_opts["verinice_extid_object"] = (
                self.customer + "_host" + str(host.id)
            )
            template_opts["verinice_client_name"] = client[8:]
            template_opts["verinice_client_id"] = str(host.id)
            template_opts["mon_host_obj"] = host

            client_list.append(soap_tpl.render(template_opts))

            print(soap_tpl.render(template_opts))

        return client_list

    def sync_servers(self):
        all_servers = (
            CollectAppsRel.objects.using("mon_collect")
            .filter(
                computer_id__istartswith=self.customer, computer_id__icontains="swde"
            )
            .values_list("computer_id", flat=True)
            .distinct()
        )
        server_list = list()

        soap_tpl = loader.get_template("soap_verinice_server.xml")

        for server in all_servers:
            host = self.get_or_create_mon_client(server)

            template_opts = dict()
            template_opts["verinice_source_id"] = "mon_" + self.customer
            template_opts["verinice_extid_object"] = (
                self.customer + "_host" + str(host.id)
            )
            template_opts["verinice_server_name"] = server[8:]
            template_opts["verinice_server_id"] = str(host.id)
            template_opts["mon_host_obj"] = host

            server_list.append(soap_tpl.render(template_opts))

            print(soap_tpl.render(template_opts))

        return server_list

    def sync_apps(self):
        all_apps = CollectAppsData.objects.using("mon_collect").filter(
            collectrelapps__computer_id__istartswith=self.customer
        )
        app_list = list()

        soap_tpl = loader.get_template("soap_verinice_app.xml")

        for app in all_apps:
            template_opts = dict()

            template_opts["verinice_extid_object"] = (
                self.customer + "_app" + str(app.app_id)
            )
            template_opts["verinice_app_name"] = app.app_name
            template_opts["verinice_app_id"] = str(app.app_id)

            app_list.append(soap_tpl.render(template_opts))

        return list(set(app_list))

    def sync_relations(self):
        all_app_relations = CollectAppsRel.objects.using("mon_collect").filter(
            computer_id__istartswith=self.customer
        )
        app_relation_list = list()

        soap_tpl = loader.get_template("soap_verinice_rel.xml")

        for app_rel in all_app_relations:
            host = self.get_or_create_mon_client(app_rel.computer_id)

            template_opts = dict()

            template_opts["verinice_extid_object"] = (
                self.customer + "_app_rel" + str(app_rel.rel_apps_id)
            )
            template_opts["verinice_app_rel_dependant"] = (
                self.customer + "_host" + str(host.id)
            )
            template_opts["verinice_app_rel_dependency"] = (
                self.customer + "_app" + str(app_rel.app_id)
            )

            app_relation_list.append(soap_tpl.render(template_opts))

        return app_relation_list

    def get_or_create_mon_client(self, host_name_custnr: str):
        session = requests.Session()
        session.headers = {
            "Accept": "application/json",
        }

        session.auth = (
            settings.ICINGA_DIRECTOR_API_USER,
            settings.ICINGA_DIRECTOR_API_PASSWD,
        )

        try:
            host = IcingaHost.objects.using("icinga_director").get(
                object_name=host_name_custnr
            )
        except IcingaHost.DoesNotExist:
            new_host = dict()
            new_host["accept_config"] = True
            new_host["address"] = "127.0.0.1"
            new_host["address6"] = "::1"
            new_host["display_name"] = host_name_custnr
            new_host["has_agent"] = True
            new_host["imports"] = list()

            new_host["imports"].append("sl-new-host")

            new_host["master_should_connect"] = False
            new_host["object_name"] = host_name_custnr
            new_host["object_type"] = "object"
            new_host["vars"] = dict()
            new_host["vars"]["check_up_down"] = False

            print(json.dumps(new_host))

            request_url = urljoin(settings.ICINGA_DIRECTOR_API_URL, "director/host")

            request_args = {"url": request_url, "json": new_host, "verify": False}

            response = session.post(**request_args)

        host = IcingaHost.objects.using("icinga_director").get(
            object_name=host_name_custnr
        )

        return host

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        soap_tpl = loader.get_template("soap_verinice.xml")

        all_mon_customers = CusGroups.objects.using("icinga_main").exclude(
            sync_verinice=False
        )

        for mon_cust in all_mon_customers:
            self.customer = mon_cust.name

            template_opts = dict()
            template_opts["verinice_source_id"] = "mon_" + self.customer

            sync_objects = list()

            sync_objects = sync_objects + self.sync_clients()
            sync_objects = sync_objects + self.sync_servers()
            sync_objects = sync_objects + self.sync_apps()
            sync_objects = sync_objects + self.sync_relations()

            # drop duplicate objects
            template_opts["sync_objects"] = sync_objects

            file = open("testfile.txt", "w")
            file.write(soap_tpl.render(template_opts))
            file.close()

            r = requests.post(
                self.api_url,
                auth=HTTPBasicAuth(settings.VERINICE_USER, settings.VERINICE_PASSWD),
                data=soap_tpl.render(template_opts).encode("utf-8"),
            )

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
